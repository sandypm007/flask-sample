from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app import app, login
from app import db
from app.forms import LoginForm, RegisterForm
from app.models import User


@app.route('/')
@login_required
def index():
    return render_template('index.html')


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/user/<user_id>')
@login_required
def hello(user_id):
    user = User.query.get(int(user_id))
    return render_template('hello.html', title='Template', user=user)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))

    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        app.logger.info('Login attempt for {username}'.format(username=form.username.data))
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            app.logger.info('Failed login attempt for {username}'.format(username=form.username.data))
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('hello', user_id=user.id)
        app.logger.info('Succeeded login attempt for {username}'.format(username=form.username.data))
        return redirect(next_page)
    return render_template('login.html', title='Login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
