import os

from dotenv import load_dotenv

load_dotenv()


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(object):
    SECRET_KEY = "{key}_test".format(key=os.environ.get('SECRET_KEY'))
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    testing = True
    debug = True
