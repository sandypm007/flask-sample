import unittest

from flask_fixtures import FixturesMixin

from app import app
from app.config import TestConfig
from app.models import db, User

app.config.from_object(TestConfig)


class TestUserFixtures(unittest.TestCase, FixturesMixin):
    fixtures = ['users.json']

    app = app
    db = db

    def test_users(self):
        users = User.query.all()
        assert len(users) == User.query.count() == 1
